#include <Arduino.h>
#include <ir_func.h>
#include <motor_func.h>
uint8_t ANALOG_PINS [14] = {A0, A1, A2, A3, A4, A5, A6, A7, A8, A9, A10, A11, A12, A13};
int TEMP_IR_VALUES [14] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int IR_ANGLES [14] = {50,70,90,110,130,150,170,190,210,230,250,270,290,310};
int TEMP_VALUES_ANGLES [14][2] = {{0,50}, {0,70}, {0, 90}, {0,110}, {0,130}, {0,150}, {0,170}, {0,190}, {0,210}, {0,230}, {0,250}, {0,290}, {0,310}};
int get_ir_value(int irsensor){
    int PIN = irsensor;
    int val = 0;
    for (int i = 0; i < 50; i++){
        val += analogRead(PIN);
        val = val/50;
    }
    val -= 6;
    return val;
}
int getBallAngle(int TEMP_IR_VALUES[14]){
    int smallest_ir = smallestvalue(TEMP_IR_VALUES);
    int angle = 0;
    int counter = 0;
    int front = get_ir_value(A14);
    for (int i = 0; i < 14; i++){
      if (TEMP_IR_VALUES[i] == smallest_ir){
        angle += IR_ANGLES[i];
        counter+=1;
      }
    }
    angle /= counter;
    if(front<smallest_ir){
      angle = 0;
    }
    return angle;
}
int getBallSensor(){
  int val =0;
  for (int i = 0; i <50; i++){
    val +=analogRead(A14);
  }
  return abs(val/50);
}