#include <Arduino.h>
#include <Wire.h>
#include "motor_func.h"
#include "ir_func.h"
#include "yaw_func.h"
#include "line_func.h"
#include "Adafruit_MCP3008.h"
Adafruit_MCP3008 mcp3008;
int angle, counter, front_sensor, smallest, motor_value=0, laser;
float yaw;
void setup() {
  Wire.begin();
  Serial.begin(9600);
  bno_setup();
  mcp3008.begin(52, 51, 50, 53);
}
void loop() {
  for (int i = 0; i<14; i++){
    TEMP_IR_VALUES[i] = get_ir_value(ANALOG_PINS[i]);
  }
  for (int i = 0; i<4; i++){
    TEMP_LINE_VALUES[i] = get_color(i);
  }
  front_sensor = get_ir_value(A14);
  yaw = get_yaw();
  angle = getBallAngle(TEMP_IR_VALUES);
  smallest = smallestvalue(TEMP_IR_VALUES);
  laser = mcp3008.readADC(7);
  if (front_sensor<=smallest&&laser>60){
    motor_value = 1;
  }
  else if(front_sensor<=smallest&&laser<60){
    if (yaw<357.5||yaw>3.5){
      if (yaw<=180){
        motor_value = 5;
      }
      else{
        motor_value = 6;
      }
    }
    else{
      motor_value = 4;
    }
    
  }
  else if (angle<310||angle>50){
    if (angle <= 180){
      motor_value = 2;
    }
    else{
      motor_value = 3;
    }
  }
  if (motor_value==1);{
    forward(50,0);
  }
  if (motor_value==2){
    turn_right_lite(50,0);
  }
  if (motor_value==3){
    turn_left_lite(50,0);
  }
  if (motor_value==4){
    forward(50,100);
  }
  if (motor_value==5){
    turn_right_lite(50,100);
  }
  if (motor_value==6){
    turn_left_lite(50,100);
  }
  

// Once laser <60, turn to 0 on compass sensor
// check compass sensor again to make sure its at 0, then kick until laser > 60. 
// make a turn to ball, once front ir sensors are greatest, go forward for 2 seconds, or until laser < 60
// OPTIONAL: if laser <60 and compass is from 100-260, turn at half speed and dribbler full power so ball doesnt fly out
//if line sensor goes off, turn 180 and go forward 1 second

}