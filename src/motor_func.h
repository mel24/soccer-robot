#ifndef MOTOR_FUNC_H
#define MOTOR_FUNC_H
#include <Wire.h>
struct Motor{
  int forward;
  int backward;
};
extern int speed;
Motor createMotor(int forward, int backwards);
extern Motor rightTop;
extern Motor leftTop;
extern Motor rightBottom;
extern Motor leftBottom;
void backward(int speed, int dribbler);
void forward(int speed, int dribbler);
void turnright(int angleToTurn, float currentAngle, int speed, int dribbler);
void turnleft(int angleToTurn, float currentAngle, int speed ,int dribbler);
void stopmotors();
void goLeft(int speed, int dribbler);
void goRight(int speed, int dribbler);
int smallestvalue(int values[14]);
void turnToball(int speed);
void turn_right_lite(int speed, int dribbler);
void turn_left_lite(int speed, int dribbler);
void testmotors();
#endif