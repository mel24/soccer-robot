#ifndef IR_FUNC_H
#define IR_FUNC_H
extern int IR_ANGLES [14];
extern uint8_t ANALOG_PINS [14];
extern int TEMP_IR_VALUES [14];
extern int PIN;
extern int val;
int get_ir_value(int irsensor);
int getBallAngle(int TEMP_IR_VALUES[14]);
int getBallSensor();
#endif