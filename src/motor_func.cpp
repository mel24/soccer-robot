#include <Arduino.h>
#include <Wire.h>
#include "motor_func.h"
#include "yaw_func.h"
#include "ir_func.h"
Motor createMotor(int forward, int backwards){
  Motor motor;  
  motor.forward = forward;
  motor.backward = backwards; 
  return motor;
};
Motor rightTop = createMotor(0x80, 0);
Motor leftTop = createMotor(0x80, 0);
Motor rightBottom = createMotor(0x80, 0);
Motor leftBottom = createMotor(0, 0x80);
int smallestvalue(int Arr[14]){
	int small=Arr[0];
	for(int i=1;i<14;i++)
	{
		if(Arr[i]<small)
			small=Arr[i];
	} 
	return small;
}
void turnToBall(int speed){
  int front_ir = get_ir_value(A14), smallest;
  for(int i; i<14;i++){
    TEMP_IR_VALUES[i] = get_ir_value(ANALOG_PINS[i]);
  };
  smallest = smallestvalue(TEMP_IR_VALUES);
  while (front_ir<=smallest){
    int smallest;
    front_ir = get_ir_value(A14);
    for(int i; i<14;i++){
      TEMP_IR_VALUES[i] = get_ir_value(ANALOG_PINS[i]);
    };
    smallest = smallestvalue(TEMP_IR_VALUES);
    float currentangle = get_yaw();
    int angle = getBallAngle(TEMP_IR_VALUES);
    if (angle<180){
      turnright(angle, currentangle, 10,0);
    }
    else{
      turnleft(angle, currentangle, 10,0);
    }
  }
}

void stopmotors(){
  Wire.beginTransmission(0x0A);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
}



void goLeft(int speed, int dribbler){
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.forward+speed);
  Wire.write(leftTop.backward+speed);
  Wire.write(rightBottom.backward+speed);
  Wire.write(leftBottom.forward+speed);
  Wire.write(dribbler);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
}



void goRight(int speed, int dribbler){
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.backward+speed);
  Wire.write(leftTop.forward+speed);
  Wire.write(rightBottom.forward+speed);
  Wire.write(leftBottom.backward+speed);
  Wire.write(dribbler);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
}



void backward(int speed, int dribbler){
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.backward+speed);
  Wire.write(leftTop.backward+speed);
  Wire.write(rightBottom.backward+speed);
  Wire.write(leftBottom.backward+speed);
  Wire.write(dribbler);
  for (int i = 0; i<int(2); i++){
    Wire.write(0);
  };
  Wire.endTransmission();
};



void forward(int speed, int dribbler){
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.forward+speed);
  Wire.write(leftTop.forward+speed);
  Wire.write(rightBottom.forward+speed);
  Wire.write(leftBottom.forward+speed);
  Wire.write(dribbler);
  for (int i = 0; i<int(2); i++){
      Wire.write(0);
  };
  Wire.endTransmission();
};

void turnleft(int angleToTurn, float currentAngle, int speed, int dribbler){
  float toTurnTo;
  if (currentAngle+angleToTurn<0){
    toTurnTo = (currentAngle+angleToTurn) + 360;
  }
  else if(currentAngle+angleToTurn>360){
    toTurnTo = (currentAngle+angleToTurn)-360;
  }
  else{
    toTurnTo = (currentAngle+angleToTurn);
  }
  float yaw = currentAngle;
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.forward+speed);
  Wire.write(leftTop.backward+speed);
  Wire.write(rightBottom.forward+speed);
  Wire.write(leftBottom.backward+speed);
  Wire.write(dribbler);
  for (int i = 0; i<int(2); i++){
    Wire.write(0);
  };
  Wire.endTransmission();
  while((yaw<toTurnTo-1.5) || (yaw>toTurnTo+1.5)){
    yaw = get_yaw();
  };
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.backward+speed);
  Wire.write(leftTop.forward+speed);
  Wire.write(rightBottom.backward+speed);
  Wire.write(leftBottom.forward+speed);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(100);
  stopmotors();
};

void turnright(int angleToTurn, float currentAngle, int speed, int dribbler){
  float toTurnTo;
  if (currentAngle+angleToTurn<0){
    toTurnTo = (currentAngle+angleToTurn) + 360;
  }
  else if(currentAngle+angleToTurn>360){
    toTurnTo = (currentAngle+angleToTurn)-360;
  }
  else{
    toTurnTo = (currentAngle+angleToTurn);
  }
  float yaw = currentAngle;
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.backward+speed);
  Wire.write(leftTop.forward+speed);
  Wire.write(rightBottom.backward+speed);
  Wire.write(leftBottom.forward+speed);
  Wire.write(dribbler);
  for (int i = 0; i<int(2); i++){
    Wire.write(0);
  };
  Wire.endTransmission();
  while((yaw<toTurnTo-1.5) || (yaw>toTurnTo+1.5)){
    yaw = get_yaw();
  };
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.forward+speed/2);
  Wire.write(leftTop.backward+speed/2);
  Wire.write(rightBottom.forward+speed/2);
  Wire.write(leftBottom.backward+speed/2);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(100);
  stopmotors();
};


void turn_right_lite(int speed, int dribbler){
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.backward+speed);
  Wire.write(leftTop.forward+speed);
  Wire.write(rightBottom.forward+speed);
  Wire.write(leftBottom.backward+speed);
  Wire.write(dribbler);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
}

void turn_left_lite(int speed, int dribbler){
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.forward+speed);
  Wire.write(leftTop.backward+speed);
  Wire.write(rightBottom.backward+speed);
  Wire.write(leftBottom.forward+speed);
  Wire.write(dribbler);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
}
void testmotors(){
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.forward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  Wire.beginTransmission(0x0A);
  Wire.write(rightTop.backward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  Wire.beginTransmission(0x0A);
  Wire.write(0);
  Wire.write(leftTop.forward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  Wire.beginTransmission(0x0A);
  Wire.write(0);
  Wire.write(leftTop.backward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  Wire.beginTransmission(0x0A);
  Wire.write(0);
  Wire.write(0);
  Wire.write(rightBottom.forward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  Wire.beginTransmission(0x0A);
  Wire.write(0);
  Wire.write(0);
  Wire.write(rightBottom.backward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  Wire.beginTransmission(0x0A);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(leftBottom.forward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  Wire.beginTransmission(0x0A);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.write(leftBottom.backward+10);
  Wire.write(0);
  Wire.write(0);
  Wire.write(0);
  Wire.endTransmission();
  delay(500);
  stopmotors();
}