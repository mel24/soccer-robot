#ifndef YAW_FUNC_H
#define YAW_FUNC_H
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
extern Adafruit_BNO055 bno;
float get_yaw();
void bno_setup();
#endif