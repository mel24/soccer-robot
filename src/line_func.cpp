#include <Arduino.h>
#include <Adafruit_MCP3008.h>
#include <line_func.h>
Adafruit_MCP3008 mcp;
int TEMP_LINE_VALUES[4] = {0, 0, 0, 0};
int get_color(int sensor_number){
    int raw_value = mcp.readADC(sensor_number);
    if (raw_value >= 100){ //white line value
        return 0; //white lines
    }
    else if (raw_value >= 700){
        return 1; //green carpet
    }
    else if (raw_value >= 900 && raw_value <= 1500){
        return 2; //black lines and dots
    }
    else{
        return 69; //this should not happen
    }
}