#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#include <yaw_func.h>
void bno_setup()
{
  Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28);
  bno.begin();
  bno.setExtCrystalUse(true);
}
float get_yaw(){
  Adafruit_BNO055 bno = Adafruit_BNO055(-1, 0x28);
  imu::Vector<3> euler = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  float temp = euler.x();
  uint8_t system, gyro, accel, mag = 0;
  bno.getCalibration(&system, &gyro, &accel, &mag);
  return temp;
}